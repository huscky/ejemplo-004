<?php
    /*
     * Ejemplo de etiqueta a crear
     * <div style="background-color: coral;
     *              width:400px;text-align: center;margin:10px auto">
     * </div>
     */

    function salidaCentrada($texto,$ancho=400){
        $estilos="width:{$ancho}px"; // colocamos el ancho
        $estilos=$estilos . ";background-color:coral"; // colocamos el color azul
        $estilos=$estilos . ";margin:10px auto"; // colocamos la caja centrada
        $estilos=$estilos . ";text-align:center"; // colocamos el texto centrado
        $etiqueta='<div style="' . $estilos .  '">' . "{$texto}</div>";
        
        return $etiqueta;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        echo salidaCentrada("Ejemplo de clase");
        echo salidaCentrada("Ejemplo de 300",200);
        ?>
    </body>
</html>

