<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            // funciones callbacks
            $numeros=[1,2,4];
            $salida=array_map(function($valor){
                return $valor**2; 
            },$numeros);
            var_dump($salida);
            
            // funcion callbacks con argumentos
            $n=3;
            $salida=array_map(function($valor) use($n){
                return $valor**$n; 
            },$numeros);
            var_dump($salida);
        ?>
    </body>
</html>
