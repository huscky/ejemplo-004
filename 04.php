<?php
function mostrar($etiqueta,$mensaje){
    echo "<{$etiqueta}>{$mensaje}</{$etiqueta}>";
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        mostrar("h1","hola");
        mostrar("h2","clase");
        $etiqueta="div";
        $texto="Hola clase";
        mostrar($etiqueta,$texto);
        ?>
    </body>
</html>
